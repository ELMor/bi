package es.olmo.ib3;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

public class RootBackupManager implements Serializable,Iterable<BackupManager>{
	private static final long serialVersionUID = 1054490933911459644L;
	
	private File rootBackupFile;
	Vector<BackupManager> allBUMs=new Vector<>();
	
	public RootBackupManager(File rbum){
		if(!isDir(rbum,true))
			;
		rootBackupFile=rbum;
	}
	public BackupManager getBUM(String relPath) throws Exception{
		File BUMFile=new File(rootBackupFile,relPath);
		if(!isDir(BUMFile,false))
			return null;
		return new BackupManager(this, relPath);
	}
	public BackupManager createBUM(String relPath) throws Exception{
		File BUMFile=new File(rootBackupFile,relPath);
		BUMFile.mkdirs();
		return new BackupManager(this, relPath);
	}
	private boolean isDir(File rbum, boolean thro){
		if(!rbum.exists() || !rbum.isDirectory()){
			if(thro)
				throw new RuntimeException(rbum.getAbsolutePath()+" Is not a dir or does not exist.");
			else
				return false;
		}
		return true;
	}
	public File getRoot(){
		return rootBackupFile;
	}
	public void log(String txt,String rp){
		log(txt,rp,null);
	}
	public void log(String opt, String rp, String rest[]){
		try {
			File logFile=new File(rootBackupFile,"OpLog.txt");
			FileWriter fw=new FileWriter(logFile, true);
			String date = Statics.getCurrentDate();
			fw.write(date);
			fw.write(" ");
			fw.write(opt);
			fw.write(" '");
			fw.write(rp);
			fw.write("'\n");
			if(rest!=null)
				for(String s:rest)
					fw.write(s+"\n");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadAll(File rbf){
		try {
			File list[]=rbf.listFiles();
			for(File subdir:list){
				if(!subdir.isDirectory())
					continue;
				File sublist[]=subdir.listFiles(new ISBum());
				if(sublist!=null && sublist.length>0){
					String relativePath=subdir.getAbsolutePath();
					int initRel=1+rootBackupFile.getAbsolutePath().length();
					relativePath=relativePath.substring(initRel);
					allBUMs.add(new BackupManager(this, relativePath));
				}
				loadAll(subdir);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			allBUMs=new Vector<>();
		}
	}
	@Override
	public Iterator<BackupManager> iterator() {
		if(allBUMs.size()==0)
			loadAll(rootBackupFile);
		return allBUMs.iterator();
	}
	
	public BackupManager initBUM(String logTxt, String relPath) throws Exception {
		BackupManager bum=getBUM(relPath);
		if(bum==null){
			bum=createBUM(relPath);
			log("Creating BUM ",relPath);
		}
		if(bum==null)
			throw new RuntimeException(relPath+" is not a backup location.");
		log(logTxt,relPath);
		return bum;
	}
	
	public void clearSTATs(){
		if(allBUMs.size()==0)
			loadAll(rootBackupFile);
		for(BackupManager bum:allBUMs)
			bum.getStatFile().delete();
	}
	
	class ISBum implements FilenameFilter {
		@Override
		public boolean accept(File dir, String name) {
			String s=name.toLowerCase();
			if(s.endsWith(Statics.snapExt))
				return true;
			if(s.equals(Statics.statName.toLowerCase()))
				return true;
			return false;
		}
	}
}
